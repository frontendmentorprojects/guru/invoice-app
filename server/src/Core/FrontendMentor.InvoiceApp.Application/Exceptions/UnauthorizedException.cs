namespace FrontendMentor.InvoiceApp.Application.Exceptions;

public sealed class UnauthorizedException : Exception { }
