﻿using System.Text.Json.Serialization;

namespace FrontendMentor.InvoiceApp.BFF.Authentication;

public sealed record LoginResponse([property: JsonPropertyName("token")] string Token, [property: JsonPropertyName("refreshToken")] string RefreshToken);
