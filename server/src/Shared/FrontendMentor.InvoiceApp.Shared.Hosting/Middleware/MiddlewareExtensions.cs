using Microsoft.AspNetCore.Builder;

namespace FrontendMentor.InvoiceApp.Shared.Hosting.Middleware;

public static class MiddlewareExtensions
{
    public static IApplicationBuilder UseApiExceptionHandler(this IApplicationBuilder builder)
    {
        return builder.UseMiddleware<ExceptionHandlerMiddleware>();
    }
}
