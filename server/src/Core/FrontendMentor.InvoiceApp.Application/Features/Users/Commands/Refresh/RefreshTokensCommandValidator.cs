using FluentValidation;

namespace FrontendMentor.InvoiceApp.Application.Features.Users.Commands.Refresh;

public sealed class RefreshTokensCommandValidator : AbstractValidator<RefreshTokensCommand>
{
    public RefreshTokensCommandValidator()
    {
        RuleFor(c => c.AccessToken).NotEmpty();
        RuleFor(c => c.RefreshToken).NotEmpty();
    }
}
