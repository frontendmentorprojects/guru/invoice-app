export interface FieldProps<TValue> {
  name: string;
  label?: string;
  validate?: (value: TValue) => string;
  fullWidth?: boolean;
}
