﻿using FrontendMentor.InvoiceApp.Application.Models.Authentication;
using MediatR;

namespace FrontendMentor.InvoiceApp.Application.Features.Users.Queries.GetProfile;

public sealed record GetProfileQuery: IRequest<ProfileResponse>;
