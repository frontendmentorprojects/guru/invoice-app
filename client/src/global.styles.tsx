import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  :root {
    --clr-primary-200: hsl(231, 73%, 93%);
    --clr-primary-300: hsl(252, 100%, 73%);
    --clr-primary-400: hsl(252, 94%, 67%);
    --clr-primary-700: hsl(233, 31%, 17%);
    --clr-primary-900: hsl(233, 30%, 21%);

    --clr-accent-300: hsl(0, 100%, 80%);
    --clr-accent-400: hsl(0, 80%, 63%);
    --clr-accent-500: hsl(160, 67%, 52%);
    --clr-accent-600: hsl(34, 100%, 50%);
    --clr-accent-700: hsl(231, 67%, 99%);
    
    --clr-neutral-50: hsl(0, 0%, 100%);
    --clr-neutral-100: hsl(240, 27%, 98%);
    --clr-neutral-200: hsl(231, 73%, 93%);
    --clr-neutral-300: hsl(231, 20%, 61%);
    --clr-neutral-400: hsl(232, 20%, 36%);
    --clr-neutral-500: hsl(231, 37%, 63%);
    --clr-neutral-600: hsl(231, 20%, 27%);
    --clr-neutral-700: hsl(233, 30%, 11%);
    --clr-neutral-800: hsl(228, 29%, 7%);
  } 
  
  html {
    box-sizing: border-box;
  }

  body {
    font-family: 'League Spartan', Arial, Helvetica, sans-serif;
    font-weight: 700;
    color: ${({ theme }) => `${theme.primaryTextColor}`};
    line-height: 1.5;
    -webkit-font-smoothing: antialiased;
  }

  html, body {
    height: 100%;
  }

  *, *::before, *::after {
    box-sizing: inherit;
    padding: 0;
    margin: 0;
  }

  img, picture, video, canvas, svg {
    display: inline-block;
    max-width: 100%;
  }

  input, button, select, textarea {
    font: inherit;
  }

  p, h1, h2, h3, h4, h5, h6 {
    overflow-wrap: break-word;
  }

  #root {
    isolation: isolate;
  }

  a:link, &:visited {
    color: var(--clr-primary-400);
    text-decoration: none;
  }

  a:hover, a:active {
    text-decoration: underline;
    opacity: 0.8;
  }

  .text-center {
    text-align: center;
  }

  .flex {
    display: flex;
  }

  .justify-end {
    justify-content: end;
  }
`;
