import styled from 'styled-components';

type ButtonProps = {
  busy?: boolean;
};

export const BaseButton = styled.button<ButtonProps>`
  border: none;
  outline: none;
  border-radius: 10rem;
  padding: 1rem 2.5rem;
  display: flex;

  &:hover {
    cursor: pointer;
  }

  &:disabled,
  &:disabled:hover {
    cursor: not-allowed;
    opacity: 0.6;
  }
`;

export const PrimaryButton = styled(BaseButton)`
  background-color: var(--clr-primary-400);
  color: var(--clr-neutral-100);
  font-weight: 500;
  transition: all 0.3s;

  &:hover {
    background-color: var(--clr-primary-300);
  }
`;
