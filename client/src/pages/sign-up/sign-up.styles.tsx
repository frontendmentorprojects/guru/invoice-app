import styled from 'styled-components';

export const SignUpContainer = styled.div`
  min-height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 95%;
  max-width: 800px;
  margin-inline: auto;
  padding: 1rem;
`;
