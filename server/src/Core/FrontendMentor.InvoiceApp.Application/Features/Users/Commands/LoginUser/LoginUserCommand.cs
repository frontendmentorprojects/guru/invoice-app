using FrontendMentor.InvoiceApp.Application.Models.Authentication;
using MediatR;

namespace FrontendMentor.InvoiceApp.Application.Features.Users.Commands.LoginUser;

public sealed record LoginUserCommand(string Email, string Password): IRequest<LoginResponse>;
