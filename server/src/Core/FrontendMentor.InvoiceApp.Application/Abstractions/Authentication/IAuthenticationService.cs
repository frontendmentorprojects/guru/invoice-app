using FrontendMentor.InvoiceApp.Application.Models.Authentication;

namespace FrontendMentor.InvoiceApp.Application.Abstractions.Authentication;

public interface IAuthenticationService
{
    ProfileResponse GetProfile();
    Task<LoginResponse> LoginAsync(LoginRequest request);
    Task<LoginResponse> LoginExternalAsync(LoginExternalRequest request);
    Task RegisterAsync(RegisterRequest request);
    Task<LoginResponse> RefreshAsync(RefreshRequest request);
}
