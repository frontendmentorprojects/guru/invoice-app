﻿namespace FrontendMentor.InvoiceApp.BFF.Authentication;

public sealed record LoginRequest(string Email, string Password);
