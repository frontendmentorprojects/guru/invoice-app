import styled from 'styled-components';

export const FormHeading = styled.h2`
  font-size: 1.5rem;
  font-weight: 700;
`;
