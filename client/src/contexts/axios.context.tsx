import axios, {
  AxiosInstance,
  AxiosInterceptorOptions,
  AxiosResponse,
  CreateAxiosDefaults,
  InternalAxiosRequestConfig
} from 'axios';
import React from 'react';

type AxiosInstanceProviderProps = {
  config: CreateAxiosDefaults;
  requestInterceptors?: {
    (value: InternalAxiosRequestConfig): InternalAxiosRequestConfig | Promise<InternalAxiosRequestConfig>;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    onRejected: (error: any) => any;
    options: AxiosInterceptorOptions;
  }[];
  responseInterceptors?: {
    (value: AxiosResponse): AxiosResponse | Promise<AxiosResponse>;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    onRejected: (error: any) => any;
    options: AxiosInterceptorOptions;
  }[];
  children: React.ReactNode;
};

const AxiosContext = React.createContext<AxiosInstance | undefined>(undefined);
export function AxiosInstanceProvider({
  config,
  requestInterceptors,
  responseInterceptors,
  children
}: AxiosInstanceProviderProps) {
  const instanceRef = React.useRef<AxiosInstance>(axios.create(config));

  React.useEffect(() => {
    if (requestInterceptors) {
      // eslint-disable-next-line unicorn/no-array-for-each
      requestInterceptors.forEach(interceptor => instanceRef.current.interceptors.request.use(interceptor));
    }

    if (responseInterceptors) {
      // eslint-disable-next-line unicorn/no-array-for-each
      responseInterceptors.forEach(interceptor => instanceRef.current.interceptors.response.use(interceptor));
    }
  }, [requestInterceptors, responseInterceptors]);

  return <AxiosContext.Provider value={instanceRef.current}>{children}</AxiosContext.Provider>;
}

export function useAxios() {
  const context = React.useContext(AxiosContext);
  if (context === undefined) throw new Error('useAxios cannot be used outside of an AxiosInstanceProvider');

  return context;
}
