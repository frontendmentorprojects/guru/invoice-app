import styled from 'styled-components';

export const FormGroup = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 1.25rem;
  margin-bottom: 1rem;
`;
