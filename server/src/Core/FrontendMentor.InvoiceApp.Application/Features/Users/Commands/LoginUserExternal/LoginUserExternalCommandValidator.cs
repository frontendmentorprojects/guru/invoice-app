using FluentValidation;

namespace FrontendMentor.InvoiceApp.Application.Features.Users.Commands.LoginUserExternal;

public sealed class LoginUserExternalCommandValidator : AbstractValidator<LoginUserExternalCommand>
{
    public LoginUserExternalCommandValidator()
    {
        RuleFor(c => c.ProviderKey).NotEmpty();
        RuleFor(c => c.FirstName).NotEmpty();
        RuleFor(c => c.LastName).NotEmpty();
        RuleFor(c => c.Email).NotEmpty();
    }
}
