﻿namespace FrontendMentor.InvoiceApp.BFF.Authentication;

public sealed record ProfileResponse(string UserId, string FirstName, string LastName, string Email, string Avatar);
