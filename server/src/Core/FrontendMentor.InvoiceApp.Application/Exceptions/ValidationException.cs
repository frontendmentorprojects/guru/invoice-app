using FluentValidation.Results;

namespace FrontendMentor.InvoiceApp.Application.Exceptions;

public sealed class ValidationException : Exception
{
    public IReadOnlyList<ValidationFailure> ValidationErrors { get; }

    public ValidationException(IEnumerable<ValidationFailure> errors)
    {
        ValidationErrors = errors.ToList();
    }
}
