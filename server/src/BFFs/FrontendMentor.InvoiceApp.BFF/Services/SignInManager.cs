﻿using System.Security.Claims;
using FrontendMentor.InvoiceApp.BFF.Utility;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace FrontendMentor.InvoiceApp.BFF.Services;

public sealed class SignInManager : ISignInManager
{
    private readonly IHttpContextAccessor _httpContextAccessor;

    public SignInManager(IHttpContextAccessor httpContextAccessor)
    {
        _httpContextAccessor = httpContextAccessor;
    }

    public async Task SignInAsync(string accessToken, string refreshToken)
    {
        var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
        var properties = new AuthenticationProperties();
        properties.StoreTokens(new[]
        {
            new AuthenticationToken { Name = Constants.AccessToken, Value = accessToken },
            new AuthenticationToken { Name = Constants.RefreshToken, Value = refreshToken }
        });

        await _httpContextAccessor.HttpContext!.SignInAsync(new ClaimsPrincipal(identity), properties);
    }

    public async Task SignOutAsync(string scheme) => await _httpContextAccessor.HttpContext!.SignOutAsync(scheme);
}
