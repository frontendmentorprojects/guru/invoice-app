using FrontendMentor.InvoiceApp.Identity.Models;

namespace FrontendMentor.InvoiceApp.Identity.Services;

public interface IJwtSettingsRetriever
{
    JwtSettings Retrieve();
}
