﻿using FrontendMentor.InvoiceApp.Identity.Authorization;
using FrontendMentor.InvoiceApp.Identity.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using AuthenticationService = FrontendMentor.InvoiceApp.Identity.Services.AuthenticationService;
using IAuthenticationService = FrontendMentor.InvoiceApp.Application.Abstractions.Authentication.IAuthenticationService;

namespace FrontendMentor.InvoiceApp.Identity;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection ConfigureIdentity(this IServiceCollection services, string connectionString)
    {
        services.AddDbContext<InvoiceAppIdentityContext>(options =>
        {
            options.UseSqlServer(connectionString, sqlServerOptions =>
            {
                sqlServerOptions.EnableRetryOnFailure(
                    maxRetryCount: 5, TimeSpan.FromMinutes(30), errorNumbersToAdd: null);
            });
        });

        services
            .AddIdentityCore<ApplicationUser>(options =>
            {
                options.Lockout.AllowedForNewUsers = true;
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 10;
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = true;
                options.Password.RequiredLength = 6;
                options.SignIn.RequireConfirmedEmail = false;
                options.User.RequireUniqueEmail = true;
            })
            .AddEntityFrameworkStores<InvoiceAppIdentityContext>()
            .AddDefaultTokenProviders();

        services.AddSingleton<IJwtSettingsRetriever, JwtSettingsRetriever>();

        var something = services.BuildServiceProvider().GetRequiredService<IJwtSettingsRetriever>().Retrieve();

        services.AddAuthentication()
            .AddJwtBearer(options =>
            {
                options.ClaimsIssuer = something.Issuer;
                options.MapInboundClaims = false;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidIssuer = something.Issuer,
                    ValidateAudience = true,
                    ValidAudiences = something.ValidAudiences,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = something.SigningCredentials.Key,
                    RequireExpirationTime = true,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero
                };

                options.Events = new JwtBearerEvents
                {
                    OnAuthenticationFailed = context =>
                    {
                        if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                        {
                            context.Response.Headers.Add("Token-Expired", "true");
                        }

                        return Task.CompletedTask;
                    }
                };
            });

        services.AddAuthorization();

        services.AddScoped<IAuthorizationHandler, CheckCurrentUserAuthenticationHandler>();
        services.AddScoped<CurrentUser>();
        services.AddScoped<IClaimsTransformation, ClaimsTransformation>();

        services.AddScoped<ITokensGenerator, TokensGenerator>();
        services.AddScoped<IAuthenticationService, AuthenticationService>();

        return services;
    }
}
