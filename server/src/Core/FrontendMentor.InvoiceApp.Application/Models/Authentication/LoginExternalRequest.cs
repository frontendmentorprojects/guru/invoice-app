namespace FrontendMentor.InvoiceApp.Application.Models.Authentication;

public sealed record LoginExternalRequest(
    string Provider, string ProviderKey, string Email, string FirstName, string LastName);
