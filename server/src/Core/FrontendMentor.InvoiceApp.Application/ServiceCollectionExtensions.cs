using FluentValidation;
using FrontendMentor.InvoiceApp.Application.Services.Validation;
using Microsoft.Extensions.DependencyInjection;

namespace FrontendMentor.InvoiceApp.Application;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection ConfigureApplicationServices(this IServiceCollection services)
    {
        services.AddMediatR(config => config.RegisterServicesFromAssemblies(AppDomain.CurrentDomain.GetAssemblies()));
        services.AddValidatorsFromAssemblies(AppDomain.CurrentDomain.GetAssemblies());

        services.AddScoped<IValidatorProvider, ValidatorProvider>();

        return services;
    }
}
