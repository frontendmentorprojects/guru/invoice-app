using Microsoft.AspNetCore.Authorization;

namespace FrontendMentor.InvoiceApp.Identity.Authorization;

public sealed record CheckCurrentUserRequirement : IAuthorizationRequirement;

// This authorization handler verifies that the user exists even if there is a valid token
public class CheckCurrentUserAuthenticationHandler : AuthorizationHandler<CheckCurrentUserRequirement>
{
    private readonly CurrentUser _currentUser;

    public CheckCurrentUserAuthenticationHandler(CurrentUser currentUser) => _currentUser = currentUser;

    protected override Task HandleRequirementAsync(
        AuthorizationHandlerContext context, CheckCurrentUserRequirement requirement)
    {
        if (_currentUser.User is not null) context.Succeed(requirement);

        return Task.CompletedTask;
    }
}
