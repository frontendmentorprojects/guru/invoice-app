using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;

namespace FrontendMentor.InvoiceApp.Identity.Authorization;

public sealed class ClaimsTransformation : IClaimsTransformation
{
    private readonly CurrentUser _currentUser;
    private readonly UserManager<ApplicationUser> _userManager;

    public ClaimsTransformation(CurrentUser currentUser, UserManager<ApplicationUser> userManager)
    {
        _currentUser = currentUser;
        _userManager = userManager;
    }

    public async Task<ClaimsPrincipal> TransformAsync(ClaimsPrincipal principal)
    {
        // We're not going to transform anything
        // We're using this as a hook into authorization to set the current user without adding custom middleware
        _currentUser.Principal = principal;

        if (principal.FindFirstValue(JwtRegisteredClaimNames.Sub) is not { Length: > 0 } userId) return principal;

        var user = await _userManager.FindByIdAsync(userId);
        if (user is not null)
        {
            _currentUser.User = user;
        }

        return principal;
    }
}
