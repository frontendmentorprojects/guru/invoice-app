import React from 'react';
import { useQueryClient, useQuery } from 'react-query';
import { useNavigate, useLocation } from 'react-router-dom';

import { useAxios } from './axios.context';

import handleAxiosError from '../utils/handle-error';

export type SignUpRequest = {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  confirmPassword: string;
};

export type SignInRequest = Pick<SignUpRequest, 'email' | 'password'>;

type User = {
  userId: string;
  avatar: string;
} & Pick<SignUpRequest, 'email' | 'firstName' | 'lastName'>;

type AuthContextType = {
  registerUser: (request: SignUpRequest) => Promise<void>;
  signInUser: (request: SignInRequest) => Promise<void>;
  signInGoogle: () => void;
};

const AuthContext = React.createContext<AuthContextType | undefined>(undefined);
function AuthProvider({ children }: { children: React.ReactNode }) {
  const axios = useAxios();
  const queryClient = useQueryClient();
  const navigate = useNavigate();
  const location = useLocation();

  // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
  const from = (location.state?.from.pathname as string) || 'app';
  useQuery<User, Error>(
    'user',
    async () => {
      const { data } = await axios.get<User>('/auth/profile');
      return data;
    },
    { retry: false, onSuccess: () => navigate(from, { replace: true }), onError: () => navigate('/sign-in') }
  );

  const value = React.useMemo(() => {
    const registerUser = async (request: SignUpRequest) => {
      try {
        await axios.post<SignUpRequest>('/auth/register', request);
        navigate('/sign-in');
      } catch (error) {
        handleAxiosError(error);
      }
    };

    const signInUser = async (request: SignInRequest) => {
      try {
        await axios.post<SignUpRequest>('/auth/login', request);
        await queryClient.invalidateQueries();
      } catch (error) {
        handleAxiosError(error);
      }
    };

    const signInGoogle = () => {
      window.location.href = `${import.meta.env.VITE_API_BASE_URL as string}/auth/login/Google`;
    };

    return { registerUser, signInUser, signInGoogle };
  }, [axios, queryClient, navigate]);

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}

function useAuth() {
  const context = React.useContext(AuthContext);
  if (context === undefined) throw new Error('useAuth must be within an AuthProvider');

  return context;
}

export { AuthProvider, useAuth };
