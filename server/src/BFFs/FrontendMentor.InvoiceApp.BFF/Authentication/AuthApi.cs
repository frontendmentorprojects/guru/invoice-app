﻿using System.Security.Claims;
using FrontendMentor.InvoiceApp.BFF.Services;
using FrontendMentor.InvoiceApp.BFF.Utility;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http.HttpResults;

namespace FrontendMentor.InvoiceApp.BFF.Authentication;

public static class AuthApi
{
    public static RouteGroupBuilder MapAuth(this IEndpointRouteBuilder routes)
    {
        var group = routes.MapGroup("auth");

        group.MapPost("login", async (LoginRequest request, IIdentityClient identityClient, ISignInManager signInManager) =>
        {
            var (token, refreshToken) = await identityClient.LoginAsync(request);
            await signInManager.SignInAsync(token, refreshToken);
        });

        group.MapGet("login/{provider}", (string provider) =>
            Results
                .Challenge(new AuthenticationProperties { RedirectUri = $"/auth/signin/{provider}" }, new[] { provider }));

        group.MapGet("profile", async (IIdentityClient identityClient) => await identityClient.GetProfileAsync());

        group.MapPost("register",
            async Task<Results<Ok, ValidationProblem>> (RegisterRequest request, IIdentityClient identityClient) =>
            {
                var registered = await identityClient.RegisterUserAsync(request);
                if (registered)
                    return TypedResults.Ok();

                return TypedResults.ValidationProblem(new Dictionary<string, string[]>
                {
                    { "DuplicateUser", new[] { "Registration Failed. The user already exists" } }
                });
            });

        group.MapGet("signin/{provider}",
            async (string provider, HttpContext context, IIdentityClient identityClient, ISignInManager signInManager, IConfiguration configuration) =>
        {
            var result = await context.AuthenticateAsync(Constants.ExternalCookie);
            if (result.Succeeded)
            {
                var principal = result.Principal;
                var id = principal.FindFirstValue(ClaimTypes.NameIdentifier)!;
                var email = principal.FindFirstValue(ClaimTypes.Email)!;
                var firstName = principal.FindFirstValue(ClaimTypes.GivenName)!;
                var lastName = principal.FindFirstValue(ClaimTypes.Surname)!;

                var tokens = await identityClient
                    .GetOrCreateUserAsync(new ExternalLoginRequest(provider, id, email, firstName, lastName));

                await signInManager.SignInAsync(tokens.Token, tokens.RefreshToken);
            }

            await signInManager.SignOutAsync(Constants.ExternalCookie);

            return Results.Redirect($"{configuration["InvoiceAppFrontendUrl"]}/sign-in");
        });

        return group;
    }
}
