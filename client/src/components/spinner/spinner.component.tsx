import { SpinnerContainer } from './spinner.styles';

export default function Spinner() {
  return (
    <SpinnerContainer>
      <span />
      <span />
      <span />
    </SpinnerContainer>
  );
}
