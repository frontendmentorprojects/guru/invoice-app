﻿using System.Text.Json.Serialization;

namespace FrontendMentor.InvoiceApp.BFF.Authentication;

public sealed record RefreshRequest(
    [property: JsonPropertyName("accessToken")] string AccessToken, [property: JsonPropertyName("refreshToken")] string RefreshToken);
