﻿namespace FrontendMentor.InvoiceApp.BFF.Authentication;

public interface IIdentityClient
{
    Task<LoginResponse> GetOrCreateUserAsync(ExternalLoginRequest request);
    Task<LoginResponse> LoginAsync(LoginRequest request);
    Task<ProfileResponse> GetProfileAsync();
    Task<bool> RegisterUserAsync(RegisterRequest request);
}
