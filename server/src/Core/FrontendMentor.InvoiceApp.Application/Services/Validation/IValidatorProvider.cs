using FluentValidation.Results;

namespace FrontendMentor.InvoiceApp.Application.Services.Validation;

public interface IValidatorProvider
{
    IAsyncEnumerable<ValidationResult> ValidateAsync<T>(T validatable, CancellationToken cancellationToken);
}
