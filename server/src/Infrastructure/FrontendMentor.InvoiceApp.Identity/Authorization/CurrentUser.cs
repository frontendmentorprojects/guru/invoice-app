using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace FrontendMentor.InvoiceApp.Identity.Authorization;

public sealed class CurrentUser
{
    public ApplicationUser? User { get; set; }
    public required ClaimsPrincipal Principal { get; set; }

    public string Id => Principal.FindFirstValue(JwtRegisteredClaimNames.Sub)!;
}
