using FrontendMentor.InvoiceApp.Application.Models.Authentication;
using MediatR;

namespace FrontendMentor.InvoiceApp.Application.Features.Users.Commands.LoginUserExternal;

public sealed record LoginUserExternalCommand(
    string Provider, string ProviderKey, string Email, string FirstName, string LastName) : IRequest<LoginResponse>;
