using MediatR;

namespace FrontendMentor.InvoiceApp.Application.Features.Users.Commands.RegisterUser;

public sealed record RegisterUserCommand(
    string FirstName, string LastName, string Email, string Password, string ConfirmPassword): IRequest<Unit>;
