import { DefaultTheme } from 'styled-components';

declare module 'styled-components' {
  export interface DefaultTheme {
    inputBackgroundColor: string;
    inputBorderColor: string;
    mainBackgroundColor: string;
    panelBackgroundColor: string;
    primaryTextColor: string;
    secondaryTextColor: string;
  }
}

export const light: DefaultTheme = {
  inputBackgroundColor: 'var(--clr-primary-50)',
  inputBorderColor: 'var(--clr-primary-200)',
  mainBackgroundColor: 'var(--clr-neutral-100)',
  panelBackgroundColor: 'var(--clr-neutral-50)',
  primaryTextColor: 'var(--clr-neutral-800)',
  secondaryTextColor: 'var(--clr-neutral-300)'
};

export const dark: DefaultTheme = {
  inputBackgroundColor: 'var(--clr-primary-900)',
  inputBorderColor: 'var(--clr-primary-900)',
  mainBackgroundColor: 'var(--clr-neutral-700)',
  panelBackgroundColor: 'var(--clr-primary-700)',
  primaryTextColor: 'var(--clr-neutral-100)',
  secondaryTextColor: 'var(--clr-neutral-200)'
};
