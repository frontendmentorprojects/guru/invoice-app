namespace FrontendMentor.InvoiceApp.Application.Models.Authentication;

public sealed record RefreshRequest(string AccessToken, string RefreshToken);
