using FrontendMentor.InvoiceApp.Application.Abstractions.Authentication;
using FrontendMentor.InvoiceApp.Application.Models.Authentication;
using FrontendMentor.InvoiceApp.Application.Services.Validation;
using FrontendMentor.InvoiceApp.Application.Utility;
using Microsoft.Extensions.Logging;

namespace FrontendMentor.InvoiceApp.Application.Features.Users.Commands.Refresh;

public sealed class RefreshTokensCommandHandler : CommandHandler<RefreshTokensCommand, LoginResponse>
{
    private readonly IAuthenticationService _authenticationService;

    public RefreshTokensCommandHandler(ILogger<RefreshTokensCommandHandler> logger, IValidatorProvider validatorProvider, IAuthenticationService authenticationService)
        : base(logger, validatorProvider)
    {
        _authenticationService = authenticationService;
    }

    protected override async Task<LoginResponse> OnHandle(RefreshTokensCommand request, CancellationToken cancellationToken)
    {
        var (accessToken, refreshToken) = request;
        return await _authenticationService
            .RefreshAsync(new RefreshRequest(accessToken, refreshToken));
    }
}
