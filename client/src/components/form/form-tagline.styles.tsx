import styled from 'styled-components';

export const FormTagline = styled.span`
  margin-block: 0.5rem 1.5rem;
  display: inline-block;
  font-weight: 500;
`;
