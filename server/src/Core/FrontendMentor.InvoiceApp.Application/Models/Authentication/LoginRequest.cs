namespace FrontendMentor.InvoiceApp.Application.Models.Authentication;

public sealed record LoginRequest(string Email, string Password);
