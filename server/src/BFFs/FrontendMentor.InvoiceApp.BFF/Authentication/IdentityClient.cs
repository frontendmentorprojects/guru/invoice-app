﻿using System.Net.Http.Headers;
using FrontendMentor.InvoiceApp.BFF.Services;
using FrontendMentor.InvoiceApp.BFF.Utility;
using Microsoft.AspNetCore.Authentication;

namespace FrontendMentor.InvoiceApp.BFF.Authentication;

public sealed class IdentityClient : IIdentityClient
{
    private readonly HttpClient _httpClient;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly ISignInManager _signInManager;

    public IdentityClient(HttpClient httpClient, IHttpContextAccessor httpContextAccessor, ISignInManager signInManager)
    {
        _httpClient = httpClient;
        _httpContextAccessor = httpContextAccessor;
        _signInManager = signInManager;
    }

    public async Task<LoginResponse> GetOrCreateUserAsync(ExternalLoginRequest request)
    {
        var response = await _httpClient.PostAsJsonAsync("/api/users/login-external", request);
        response.EnsureSuccessStatusCode();

        return (await response.Content.ReadFromJsonAsync<LoginResponse>())!;
    }

    public async Task<ProfileResponse> GetProfileAsync()
    {
        var authenticateResult = await _httpContextAccessor.HttpContext!.AuthenticateAsync();

        var tokensMap = authenticateResult.Properties!.GetTokens().ToDictionary(token => token.Name);
        var accessToken = tokensMap[Constants.AccessToken].Value;
        var refreshToken = tokensMap[Constants.RefreshToken].Value;

        var request = new HttpRequestMessage(HttpMethod.Get, "/api/users/profile");
        request.Headers.Authorization = new AuthenticationHeaderValue(Constants.JwtAuthenticationScheme, accessToken);
        var response = await _httpClient.SendAsync(request);

        if (response.IsSuccessStatusCode)
            return (await response.Content.ReadFromJsonAsync<ProfileResponse>())!;

        var tokens = await RefreshAccessToken(accessToken, refreshToken);

        var refreshedRequest = new HttpRequestMessage(HttpMethod.Get, "/api/users/profile");
        refreshedRequest.Headers.Authorization =
            new AuthenticationHeaderValue(Constants.JwtAuthenticationScheme, tokens.Token);
        var refreshedResponse = await _httpClient.SendAsync(refreshedRequest);

        return (await refreshedResponse.Content.ReadFromJsonAsync<ProfileResponse>())!;
    }

    public async Task<LoginResponse> LoginAsync(LoginRequest request)
    {
        var response = await _httpClient.PostAsJsonAsync("/api/users/login", request);
        response.EnsureSuccessStatusCode();

        return (await response.Content.ReadFromJsonAsync<LoginResponse>())!;
    }

    public async Task<bool> RegisterUserAsync(RegisterRequest request)
    {
        var response = await _httpClient.PostAsJsonAsync("api/users/register", request);
        return response.IsSuccessStatusCode;
    }

    private async Task<LoginResponse> RefreshAccessToken(string token, string refreshToken)
    {
        var response = await _httpClient
            .PostAsJsonAsync("/api/users/refresh", new RefreshRequest(token, refreshToken));

        await _signInManager.SignInAsync(token, refreshToken);

        return (await response.Content.ReadFromJsonAsync<LoginResponse>())!;
    }
}
