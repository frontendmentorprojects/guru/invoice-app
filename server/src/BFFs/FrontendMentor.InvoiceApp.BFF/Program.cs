using FrontendMentor.InvoiceApp.BFF.Authentication;
using FrontendMentor.InvoiceApp.BFF.Services;
using FrontendMentor.InvoiceApp.BFF.Utility;
using Microsoft.AspNetCore.Authentication.Cookies;

var builder = WebApplication.CreateBuilder(args);

var allowedOrigins = builder.Configuration["AllowedOrigins"] ??
    throw new InvalidOperationException("Allowed origins is not configured");
var googleClientId = builder.Configuration["Authentication:Google:ClientId"] ??
    throw new InvalidOperationException("Google ClientId is not configured");
var googleClientSecret = builder.Configuration["Authentication:Google:ClientSecret"] ??
    throw new InvalidOperationException("Google Client Secret is not configured");
var identityUrl = builder.Configuration["FrontendMentorIdentityUrl"] ??
    throw new InvalidOperationException("Identity URL is not configured");

builder.Services
    .AddHttpContextAccessor()
    .AddCors(options =>
    {
        options.AddPolicy("InvoiceAppPolicy", policyBuilder =>
        {
            policyBuilder.WithOrigins(allowedOrigins.Split(","))
                .WithHeaders("Content-Type")
                .AllowCredentials();
        });
    })
    .AddAuthorization()
    .AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
    .AddCookie(options => options.Cookie.SameSite = SameSiteMode.None)
    .AddCookie(Constants.ExternalCookie)
    .AddGoogle(options =>
    {
        options.SignInScheme = Constants.ExternalCookie;
        options.ClientId = googleClientId;
        options.ClientSecret = googleClientSecret;
    });

builder.Services.AddHttpClient<IIdentityClient, IdentityClient>(client =>
{
    client.BaseAddress = new Uri(identityUrl);
});

builder.Services.AddScoped<ISignInManager, SignInManager>();

var app = builder.Build();

app.UseHttpsRedirection();

app.UseExceptionHandler(appBuilder => appBuilder.Run(async context =>
{
    context.Response.StatusCode = StatusCodes.Status500InternalServerError;
    await context.Response.WriteAsync("An unexpected fault occurred. Please try again later");
}));

app.UseRouting();

app.UseCors("InvoiceAppPolicy");

app.MapAuth();

app.Run();
