using FrontendMentor.InvoiceApp.Identity.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace FrontendMentor.InvoiceApp.Identity.Services;

public sealed class JwtSettingsRetriever : IJwtSettingsRetriever
{
    private readonly IConfiguration _configuration;

    public JwtSettingsRetriever(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public JwtSettings Retrieve()
    {
        var bearerSection = _configuration
            .GetSection("Authentication:Schemes:Bearer");
        var signinKeysSection = bearerSection.GetSection("SigningKeys:0");

        var issuer =
            bearerSection["ValidIssuer"] ?? throw new InvalidOperationException("Issuer is not specified");
        var signingKey =
            signinKeysSection["Value"] ?? throw new InvalidOperationException("Signing key is not specified");

        var signingCredentials =
            new SigningCredentials(
                new SymmetricSecurityKey(Convert.FromBase64String(signingKey)), SecurityAlgorithms.HmacSha256);

        var audiences = bearerSection.GetSection("ValidAudiences")
            .GetChildren()
            .Where(s => !string.IsNullOrWhiteSpace(s.Value))
            .Select(s => s.Value!)
            .ToList();

        return new JwtSettings
        {
            Issuer = issuer,
            SigningCredentials = signingCredentials,
            ValidAudiences = audiences
        };
    }
}
