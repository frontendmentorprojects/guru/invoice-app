import styled from 'styled-components';

export const Panel = styled.div`
  background-color: ${({ theme }) => theme.panelBackgroundColor};
  padding: 1.5rem;
  border-radius: 0.625rem;
  width: 100%;
`;
