using System.Net.Mime;
using System.Text.Json;
using FluentValidation.Results;
using FrontendMentor.InvoiceApp.Application.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FrontendMentor.InvoiceApp.Shared.Hosting.Middleware;

public sealed class ExceptionHandlerMiddleware
{
    private readonly RequestDelegate _next;

    public ExceptionHandlerMiddleware(RequestDelegate next) => _next = next;

    public async Task Invoke(HttpContext context)
    {
        try
        {
            await _next(context);
        }
        catch (Exception ex)
        {
            await ConvertException(context, ex);
        }
    }

    private static Task ConvertException(HttpContext context, Exception exception)
    {
        var statusCode = StatusCodes.Status500InternalServerError;
        context.Response.ContentType = MediaTypeNames.Application.Json;

        switch (exception)
        {
            case ValidationException validationException:
                statusCode = StatusCodes.Status400BadRequest;
                var validationProblemDetails =
                    new ValidationProblemDetails(CreateErrorDictionary(validationException.ValidationErrors))
                    {
                        Type = "https://tools.ietf.org/html/rfc7231#section-6.5.1",
                        Title = "One or more validation errors occurred",
                        Status = statusCode
                    };
                context.Response.StatusCode = statusCode;
                return context.Response
                    .WriteAsJsonAsync(
                        validationProblemDetails,
                        new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase });
            case UnauthorizedException:
                statusCode = StatusCodes.Status401Unauthorized;
                var unauthorizedProblemDetails = new ProblemDetails
                {
                    Type = "https://tools.ietf.org/html/rfc7231#section-3.1",
                    Title = "Unable to authenticate",
                    Status = statusCode
                };
                context.Response.StatusCode = statusCode;
                return context.Response
                    .WriteAsJsonAsync(
                        unauthorizedProblemDetails,
                        new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase });
            default:
                var problemDetails = new ProblemDetails
                {
                    Type = "https://tools.ietf.org/html/rfc7231#section-6.6.1",
                    Title = "An unexpected error occurred while processing your request",
                    Status = statusCode
                };
                context.Response.StatusCode = statusCode;
                return context.Response.WriteAsJsonAsync(problemDetails);
        }
    }

    private static IDictionary<string, string[]> CreateErrorDictionary(IEnumerable<ValidationFailure> errors)
    {
        var errorDictionary = new Dictionary<string, string[]>(StringComparer.Ordinal);

        foreach (var error in errors.GroupBy(e => e.PropertyName))
        {
            var key = error.Key;
            var failures = error.ToList();
            if (failures.Count == 1)
            {
                errorDictionary.Add(key, new [] { failures[0].ErrorMessage });
            }
            else
            {
                var errorMessages = new string[failures.Count];
                for (var i = 0; i < failures.Count; i++)
                {
                    errorMessages[i] = failures[i].ErrorMessage;
                }

                errorDictionary.Add(key, errorMessages);
            }
        }

        return errorDictionary;
    }
}
