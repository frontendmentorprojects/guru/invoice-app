﻿namespace FrontendMentor.InvoiceApp.BFF.Utility;

public static class Constants
{
    public const string JwtAuthenticationScheme = "Bearer";
    public const string ExternalCookie = "ExternalCookie";
    public const string AccessToken = "access_token";
    public const string RefreshToken = "refresh_token";
}
