using FrontendMentor.InvoiceApp.Application.Abstractions.Authentication;
using FrontendMentor.InvoiceApp.Application.Models.Authentication;
using FrontendMentor.InvoiceApp.Application.Services.Validation;
using FrontendMentor.InvoiceApp.Application.Utility;
using Microsoft.Extensions.Logging;

namespace FrontendMentor.InvoiceApp.Application.Features.Users.Commands.LoginUserExternal;

public sealed class LoginUserExternalCommandHandler : CommandHandler<LoginUserExternalCommand, LoginResponse>
{
    private readonly IAuthenticationService _authenticationService;

    public LoginUserExternalCommandHandler(ILogger<LoginUserExternalCommandHandler> logger, IValidatorProvider validatorProvider, IAuthenticationService authenticationService)
        : base(logger, validatorProvider)
    {
        _authenticationService = authenticationService;
    }

    protected override async Task<LoginResponse> OnHandle(LoginUserExternalCommand request, CancellationToken cancellationToken)
    {
        var (provider, providerKey, email, firstName, lastName) = request;
        return await _authenticationService
            .LoginExternalAsync(new LoginExternalRequest(provider, providerKey, email, firstName, lastName));
    }
}
