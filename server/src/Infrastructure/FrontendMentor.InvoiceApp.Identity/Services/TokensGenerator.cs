using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;

namespace FrontendMentor.InvoiceApp.Identity.Services;

public sealed class TokensGenerator : ITokensGenerator
{
    private readonly IJwtSettingsRetriever _jwtSettingsRetriever;

    public TokensGenerator(IJwtSettingsRetriever jwtSettingsRetriever) => _jwtSettingsRetriever = jwtSettingsRetriever;

    public (string AccessToken, string RefreshToken) GenerateTokens(ApplicationUser user)
    {
        var refreshToken = GenerateRefreshToken();

        var jwtSettings = _jwtSettingsRetriever.Retrieve();
        var claims = new List<Claim>
        {
            new(JwtRegisteredClaimNames.Sub, user.Id),
            new(JwtRegisteredClaimNames.Jti, jwtSettings.JtiGenerator()),
            new(JwtRegisteredClaimNames.Iat, jwtSettings.IssuedAt.ToUnixTimeSeconds().ToString()),
            new(JwtRegisteredClaimNames.GivenName, user.FirstName),
            new(JwtRegisteredClaimNames.FamilyName, user.LastName),
            new(JwtRegisteredClaimNames.Email, user.Email!)
        };

        claims.AddRange(jwtSettings.ValidAudiences
            .Select(s => new Claim(JwtRegisteredClaimNames.Aud, s)));

        var jwt = new JwtSecurityToken(jwtSettings.Issuer, null, claims, jwtSettings.NotBefore.UtcDateTime,
            jwtSettings.Expiration(TimeSpan.FromMinutes(5)).UtcDateTime, jwtSettings.SigningCredentials);

        return (new JwtSecurityTokenHandler().WriteToken(jwt), refreshToken);

        static string GenerateRefreshToken()
        {
            using var rng = RandomNumberGenerator.Create();

            var randomBytes = new byte[32];
            rng.GetBytes(randomBytes);

            return Convert.ToBase64String(randomBytes);
        }
    }
}
