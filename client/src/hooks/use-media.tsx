import React from 'react';

export default function useMedia<T>(queries: string[], values: T[], defaultValue: T) {
  // Array containing media query list for each query
  const mediaQueryLists = queries.map(query => window.matchMedia(query));

  // Function that gets value based on matching media query
  const getValue = () => {
    // Get index of first media query that matches
    const index = mediaQueryLists.findIndex(mql => mql.matches);

    // Return related value or default value if none
    return values?.[index] || defaultValue;
  };

  const [value, setValue] = React.useState<T>(getValue);

  React.useEffect(() => {
    const handler = () => setValue(getValue());

    // Set a listener for each media query with above handler as callback
    // eslint-disable-next-line unicorn/no-array-for-each
    mediaQueryLists.forEach(mql => mql.addEventListener('change', handler));

    // Remove listeners on cleanup
    return () => {
      // eslint-disable-next-line unicorn/no-array-for-each
      mediaQueryLists.forEach(mql => mql.removeEventListener('change', handler));
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return value;
}
