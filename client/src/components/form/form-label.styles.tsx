import styled from 'styled-components';

type LabelProps = {
  error: boolean;
};

export const FormLabel = styled.label<LabelProps>`
  font-weight: 500;
  font-size: 1.14rem;
  color: ${({ theme, error }) => (error ? 'var(--clr-accent-400)' : theme.secondaryTextColor)};
`;
