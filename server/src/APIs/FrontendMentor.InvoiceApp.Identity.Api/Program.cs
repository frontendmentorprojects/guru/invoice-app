using FrontendMentor.InvoiceApp.Application;
using FrontendMentor.InvoiceApp.Identity;
using FrontendMentor.InvoiceApp.Identity.Authorization;
using FrontendMentor.InvoiceApp.Shared.Hosting;
using FrontendMentor.InvoiceApp.Shared.Hosting.Middleware;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Serilog;
using Serilog.Formatting.Json;

Log.Logger = new LoggerConfiguration()
    .WriteTo.Console(new JsonFormatter())
    .CreateLogger();

try
{
    Log.Information("Starting Frontend Mentor Identity Service");

    var builder = WebApplication.CreateBuilder(args);
    builder.Host.UseSerilog((context, configuration) =>
        configuration.ReadFrom.Configuration(context.Configuration));

    builder.Services
        .ConfigureControllers(new CheckCurrentUserRequirement())
        .ConfigureOpenApi(new OpenApiInfo
        {
            Version = "v1",
            Title = "Frontend Mentor Identity Service",
            Description = "This document describes the Restful API of the Frontend Mentor Identity Service",
            Contact = new OpenApiContact
            {
                Name = "Denzil L. Martin",
                Email = "denzilm@example.com"
            }
        })
        .ConfigureVersioning()
        .ConfigureApplicationServices()
        .ConfigureIdentity(builder.Configuration.GetConnectionString("DefaultConnection")!);

    var app = builder.Build();

    using (var scope = app.Services.CreateScope())
    {
        try
        {
            var context = scope.ServiceProvider.GetRequiredService<InvoiceAppIdentityContext>();
            await context.Database.MigrateAsync();
        }
        catch (Exception e)
        {
            Log.Error(e, "An error occurred while migrating the database");
        }
    }

    app.UseSerilogRequestLogging();
    app.UseApiExceptionHandler();

    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }

    app.UseAuthentication();
    app.UseAuthorization();

    app.MapControllers();

    app.Run();
}
catch (Exception e)
{
    Log.Fatal(e, "Host terminated unexpectedly");
}
finally
{
    Log.CloseAndFlush();
}


