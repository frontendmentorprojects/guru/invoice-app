import * as Yup from 'yup';

export const SignUpValidationSchema = Yup.object().shape({
  firstName: Yup.string()
    .min(2, 'Must be 2 characters or more')
    .max(100, "Can't be more than 100 characters long")
    .required("Can't be empty"),
  lastName: Yup.string()
    .min(2, 'Must be 2 characters or more')
    .max(100, "Can't be more than 100 characters long")
    .required("Can't be empty"),
  email: Yup.string().email().required("Can't be empty"),
  password: Yup.string().min(6, 'Must be 6 characters or more').required("Can't be empty"),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref('password')], 'Passwords does not match')
    .required("Can't be empty")
});
