namespace FrontendMentor.InvoiceApp.Application.Models.Authentication;

public sealed record LoginResponse(string Token, string RefreshToken);
