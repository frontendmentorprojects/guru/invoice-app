import { Formik } from 'formik';
import { Link } from 'react-router-dom';

import { FaGoogle } from 'react-icons/fa6';

import { FormHeading } from '../../components/form/form-heading.styles';
import { FormTagline } from '../../components/form/form-tagline.styles';
import { Panel } from '../../components/panel/panel.styles';
import { Divider, SignInContainer, SocialButtonsContainer } from './sign-in.styles';

import { SignInRequest, useAuth } from '../../contexts/auth.context';

import { SignInValidationSchema } from './sign-in.validation';

import { FormGroup } from '../../components/form/form-group.styles';
import InputField from '../../components/form/input-field.component';
import Button, { SubmitButton } from '../../components/button/button.component';

export default function SignIn() {
  const { signInUser, signInGoogle } = useAuth();

  return (
    <SignInContainer>
      <Panel>
        <div className="text-center">
          <FormHeading>Welcome back! Please login with email and password</FormHeading>
          <FormTagline>
            Don&apos;t have an account? <Link to="/sign-up">Sign up</Link>
          </FormTagline>
        </div>
        <Formik<SignInRequest>
          initialValues={{ email: '', password: '' }}
          validationSchema={SignInValidationSchema}
          onSubmit={async (signInRequest: SignInRequest) => {
            await signInUser(signInRequest);
          }}
        >
          <>
            <FormGroup>
              <InputField name="email" label="Email" type="email" />
            </FormGroup>
            <FormGroup>
              <InputField name="password" label="Password" type="password" />
            </FormGroup>
            <div className="flex justify-end">
              <SubmitButton>Sign In</SubmitButton>
            </div>
            <Divider>
              <span />
              <span>or sign in with</span>
              <span />
            </Divider>
            <SocialButtonsContainer>
              <Button onClick={() => signInGoogle()}>
                <FaGoogle type="button" />
              </Button>
            </SocialButtonsContainer>
          </>
        </Formik>
      </Panel>
    </SignInContainer>
  );
}
