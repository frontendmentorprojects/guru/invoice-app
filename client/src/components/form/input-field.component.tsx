import { useField } from 'formik';
import { InputHTMLAttributes } from 'react';

import { FieldProps } from './field-props';
import { FormLabel } from './form-label.styles';
import { InputGroup } from './input-group.styles';
import { Error } from './form-error.styles';
import { FormInput } from './input-field.styles';

export default function InputField({
  name,
  label,
  validate,
  fullWidth = false,
  ...otherProps
}: FieldProps<string> & InputHTMLAttributes<HTMLInputElement>) {
  const [field, { error, touched }] = useField({ name, validate });

  return (
    <InputGroup fullWidth={fullWidth}>
      <FormLabel htmlFor={`${name}`} error={!!error && touched}>
        {label}
      </FormLabel>
      <FormInput id={name} error={!!error && touched} {...otherProps} {...field} />
      {error && touched && <Error>{error}</Error>}
    </InputGroup>
  );
}
