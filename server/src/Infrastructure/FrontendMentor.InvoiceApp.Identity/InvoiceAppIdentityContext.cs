using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FrontendMentor.InvoiceApp.Identity;

public sealed class InvoiceAppIdentityContext : IdentityDbContext<ApplicationUser>
{
    public InvoiceAppIdentityContext(DbContextOptions<InvoiceAppIdentityContext> options)
        : base(options) { }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        builder.ApplyConfigurationsFromAssembly(GetType().Assembly);
    }
}
