import styled from 'styled-components';

export const SignInContainer = styled.div`
  min-height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 95%;
  max-width: 800px;
  margin-inline: auto;
  padding: 1rem;
`;

export const Divider = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 1rem;
  margin-block-start: 2rem;
  font-size: 1.1rem;

  span:nth-of-type(odd) {
    display: block;
    height: 2px;
    width: 10rem;
    background-color: var(--clr-primary-200);
  }
`;

export const SocialButtonsContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-block-start: 1rem;
`;
