import styled from 'styled-components';

import { respond } from '../../mixins.styles';

type FormInputProps = {
  error: boolean;
};

export const FormInput = styled.input<FormInputProps>`
  padding-inline: 1.25rem;
  padding-block: 0.85rem;
  font-weight: 700;
  font-size: 0.75rem;
  color: ${({ theme }) => theme.inputBackgroundColor};
  border-radius: 0.25rem;
  outline: none;
  border: ${({ theme, error }) => (error ? `1px solid var(--clr-accent-400)` : `1px solid ${theme.inputBorderColor}`)};

  ${respond.desktop`
    font-size: 0.95rem;
  `}
`;
