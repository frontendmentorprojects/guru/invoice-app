using System.Net.Mime;
using System.Text.Json;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace FrontendMentor.InvoiceApp.Shared.Hosting;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection ConfigureControllers(
        this IServiceCollection services, params IAuthorizationRequirement[] requirements)
    {
        services
            .AddControllers(options =>
            {
                options.ReturnHttpNotAcceptable = true;

                // Response types that should be applied to all action methods across all controllers
                options.Filters.Add(new ProducesResponseTypeAttribute(StatusCodes.Status406NotAcceptable));
                options.Filters.Add(new ProducesResponseTypeAttribute(StatusCodes.Status500InternalServerError));

                options.Filters.Add(new ConsumesAttribute(MediaTypeNames.Application.Json));
                options.Filters.Add(new ProducesAttribute(MediaTypeNames.Application.Json));

                // All controllers will be locked down
                // Those that should not should apply the [AllowAnonymous] attribute
                var policyBuilder =
                    new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme)
                        .RequireAuthenticatedUser();

                if (requirements.Any())
                {
                    policyBuilder.AddRequirements(requirements);
                }

                options.Filters.Add(new AuthorizeFilter(policyBuilder.Build()));
            })
            .AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase);

        return services;
    }

    public static IServiceCollection ConfigureOpenApi(this IServiceCollection services, OpenApiInfo apiInfo)
    {
        services.AddEndpointsApiExplorer();

        return services.AddSwaggerGen(options =>
        {
            options.SwaggerDoc("v1", apiInfo);

            options.AddSecurityDefinition("JWT", new OpenApiSecurityScheme
            {
                Type = SecuritySchemeType.Http,
                Scheme = JwtBearerDefaults.AuthenticationScheme,
                Description = "Input JWT to access this API"
            });

            options.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "JWT"
                        }
                    },
                    new List<string>()
                }
            });
        });
    }

    public static IServiceCollection ConfigureVersioning(this IServiceCollection services)
    {
        return services
            .AddApiVersioning(options =>
            {
                options.DefaultApiVersion = new ApiVersion(1, 0);
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.ReportApiVersions = true;
                options.ApiVersionReader = new HeaderApiVersionReader("X-Version");
            });
    }
}
