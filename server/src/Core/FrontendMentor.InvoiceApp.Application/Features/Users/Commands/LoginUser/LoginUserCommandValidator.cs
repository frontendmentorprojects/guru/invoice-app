using FluentValidation;

namespace FrontendMentor.InvoiceApp.Application.Features.Users.Commands.LoginUser;

public sealed class LoginUserCommandValidator : AbstractValidator<LoginUserCommand>
{
    public LoginUserCommandValidator()
    {
        RuleFor(c => c.Email).NotEmpty();
        RuleFor(x => x.Password).NotEmpty();
    }
}
