﻿using FrontendMentor.InvoiceApp.Application.Abstractions.Authentication;
using FrontendMentor.InvoiceApp.Application.Models.Authentication;
using MediatR;

namespace FrontendMentor.InvoiceApp.Application.Features.Users.Queries.GetProfile;

public sealed class GetProfileQueryHandler : IRequestHandler<GetProfileQuery, ProfileResponse>
{
    private readonly IAuthenticationService _authenticationService;

    public GetProfileQueryHandler(IAuthenticationService authenticationService)
    {
        _authenticationService = authenticationService;
    }

    public Task<ProfileResponse> Handle(GetProfileQuery request, CancellationToken cancellationToken)
    {
        return Task.FromResult(_authenticationService.GetProfile());
    }
}
