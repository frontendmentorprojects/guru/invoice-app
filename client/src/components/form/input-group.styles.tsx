import styled from 'styled-components';

type InputGroupProps = {
  fullWidth: boolean;
};

export const InputGroup = styled.div<InputGroupProps>`
  display: flex;
  gap: 1rem;
  flex-direction: column;
  flex: ${({ fullWidth }) => (fullWidth ? '15rem' : '1rem')};
  overflow: hidden;
  position: relative;
  margin-block-end: 1.25rem;
`;
