import { Formik } from 'formik';
import { Link } from 'react-router-dom';

import { FormHeading } from '../../components/form/form-heading.styles';
import { FormTagline } from '../../components/form/form-tagline.styles';
import { Panel } from '../../components/panel/panel.styles';
import { SignUpContainer } from './sign-up.styles';

import { SignUpRequest, useAuth } from '../../contexts/auth.context';

import { SignUpValidationSchema } from './sign-up.validation';

import { FormGroup } from '../../components/form/form-group.styles';
import InputField from '../../components/form/input-field.component';
import { SubmitButton } from '../../components/button/button.component';

export default function SignUp() {
  const { registerUser } = useAuth();

  return (
    <SignUpContainer>
      <Panel>
        <div className="text-center">
          <FormHeading>Sign up with email and password</FormHeading>
          <FormTagline>
            Already have an account? <Link to="/sign-in">Sign in</Link>
          </FormTagline>
        </div>
        <Formik<SignUpRequest>
          initialValues={{ firstName: '', lastName: '', email: '', password: '', confirmPassword: '' }}
          validationSchema={SignUpValidationSchema}
          onSubmit={async (signUpRequest: SignUpRequest) => {
            await registerUser(signUpRequest);
          }}
        >
          <>
            <FormGroup>
              <InputField name="firstName" label="First Name" />
            </FormGroup>
            <FormGroup>
              <InputField name="lastName" label="Last Name" />
            </FormGroup>
            <FormGroup>
              <InputField name="email" label="Email" type="email" />
            </FormGroup>
            <FormGroup>
              <InputField name="password" label="Password" type="password" />
            </FormGroup>
            <FormGroup>
              <InputField name="confirmPassword" label="Confirm Password" type="password" />
            </FormGroup>
            <div className="flex justify-end">
              <SubmitButton>Sign Up</SubmitButton>
            </div>
          </>
        </Formik>
      </Panel>
    </SignUpContainer>
  );
}
