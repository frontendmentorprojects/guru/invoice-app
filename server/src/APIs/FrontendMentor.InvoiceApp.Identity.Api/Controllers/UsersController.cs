using FrontendMentor.InvoiceApp.Application.Features.Users.Commands.LoginUser;
using FrontendMentor.InvoiceApp.Application.Features.Users.Commands.LoginUserExternal;
using FrontendMentor.InvoiceApp.Application.Features.Users.Commands.Refresh;
using FrontendMentor.InvoiceApp.Application.Features.Users.Commands.RegisterUser;
using FrontendMentor.InvoiceApp.Application.Features.Users.Queries.GetProfile;
using FrontendMentor.InvoiceApp.Application.Models.Authentication;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FrontendMentor.InvoiceApp.Identity.Api.Controllers;

[ApiController]
[Route("api/users")]
public sealed class UsersController : ControllerBase
{
    private readonly ISender _sender;

    public UsersController(ISender sender)
    {
        _sender = sender;
    }

    [HttpPost("login", Name = "LoginUser")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [AllowAnonymous]
    public async Task<ActionResult<LoginResponse>> LoginUser(LoginUserCommand loginUserCommand)
    {
        var response = await _sender.Send(loginUserCommand);
        return Ok(response);
    }

    [HttpPost("login-external", Name = "LoginUserExternal")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [AllowAnonymous]
    public async Task<ActionResult<LoginResponse>> LoginUserExternal(LoginUserExternalCommand loginUserExternalCommand)
    {
        var response = await _sender.Send(loginUserExternalCommand);
        return Ok(response);
    }

    [HttpGet("profile", Name = "GetProfile")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<ProfileResponse>> GetProfile()
    {
        var response = await _sender.Send(new GetProfileQuery());
        return Ok(response);
    }

    [HttpPost("refresh", Name = "RefreshTokens")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [AllowAnonymous]
    public async Task<ActionResult<LoginResponse>> RefreshTokens(RefreshTokensCommand refreshTokensCommand)
    {
        var response = await _sender.Send(refreshTokensCommand);
        return Ok(response);
    }

    [HttpPost("register", Name = "RegisterUser")]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [AllowAnonymous]
    public async Task<ActionResult> RegisterUser(RegisterUserCommand registerUserCommand)
    {
        await _sender.Send(registerUserCommand);
        return NoContent();
    }
}
