import { Routes, Route, Navigate } from 'react-router-dom';

import { AppContainer } from './app.styles';

import Layout from './components/layout/layout.component';
import NotFound from './pages/not-found/not-found.component';
import SignIn from './pages/sign-in/sign-in.component';
import SignUp from './pages/sign-up/sign-up.component';

export default function App() {
  return (
    <AppContainer>
      <Routes>
        <Route path="/" element={<Navigate replace to="/sign-in" />} />
        <Route path="sign-in" element={<SignIn />} />
        <Route path="sign-up" element={<SignUp />} />
        <Route path="app" element={<Layout />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </AppContainer>
  );
}
