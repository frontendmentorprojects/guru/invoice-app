namespace FrontendMentor.InvoiceApp.Identity.Services;

public interface ITokensGenerator
{
    (string AccessToken, string RefreshToken) GenerateTokens(ApplicationUser user);
}
