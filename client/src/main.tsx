import React from 'react';
import { createRoot } from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';

import 'react-toastify/dist/ReactToastify.min.css';
import GlobalStyles from './global.styles';

import { AuthProvider } from './contexts/auth.context';
import { AxiosInstanceProvider } from './contexts/axios.context';
import { ThemeProvider } from './contexts/theme.context';

import App from './app';

const client = new QueryClient();
const root = createRoot(document.querySelector('#root') as HTMLElement);
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <ThemeProvider>
        <GlobalStyles />
        <AxiosInstanceProvider
          config={{
            baseURL: `${import.meta.env.VITE_API_BASE_URL as string}`,
            withCredentials: true,
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json; charset=utf-8'
            }
          }}
        >
          <QueryClientProvider client={client}>
            <ReactQueryDevtools initialIsOpen={false} />
            <AuthProvider>
              <App />
              <ToastContainer />
            </AuthProvider>
          </QueryClientProvider>
        </AxiosInstanceProvider>
      </ThemeProvider>
    </BrowserRouter>
  </React.StrictMode>
);
