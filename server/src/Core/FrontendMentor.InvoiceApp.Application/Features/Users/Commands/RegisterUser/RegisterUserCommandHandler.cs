using FrontendMentor.InvoiceApp.Application.Abstractions.Authentication;
using FrontendMentor.InvoiceApp.Application.Models.Authentication;
using FrontendMentor.InvoiceApp.Application.Services.Validation;
using FrontendMentor.InvoiceApp.Application.Utility;
using MediatR;
using Microsoft.Extensions.Logging;

namespace FrontendMentor.InvoiceApp.Application.Features.Users.Commands.RegisterUser;

public sealed class RegisterUserCommandHandler : CommandHandler<RegisterUserCommand, Unit>
{
    private readonly IAuthenticationService _authenticationService;

    public RegisterUserCommandHandler(ILogger<RegisterUserCommandHandler> logger, IValidatorProvider validatorProvider, IAuthenticationService authenticationService)
        : base(logger, validatorProvider)
    {
        _authenticationService = authenticationService;
    }

    protected override async Task<Unit> OnHandle(RegisterUserCommand request, CancellationToken cancellationToken)
    {
        var (firstName, lastName, email, password, _) = request;
        await _authenticationService.RegisterAsync(new RegisterRequest(firstName, lastName, email, password));

        Logger.LogInformation("User with email '{Email}' registered successfully", email);

        return Unit.Value;
    }
}
