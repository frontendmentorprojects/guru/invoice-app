﻿namespace FrontendMentor.InvoiceApp.BFF.Authentication;

public sealed record ExternalLoginRequest(string Provider, string ProviderKey, string Email, string FirstName, string LastName);
