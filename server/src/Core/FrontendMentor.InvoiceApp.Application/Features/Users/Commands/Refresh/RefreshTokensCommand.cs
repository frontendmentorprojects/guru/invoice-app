using FrontendMentor.InvoiceApp.Application.Models.Authentication;
using MediatR;

namespace FrontendMentor.InvoiceApp.Application.Features.Users.Commands.Refresh;

public sealed record RefreshTokensCommand(string AccessToken, string RefreshToken) : IRequest<LoginResponse>;
