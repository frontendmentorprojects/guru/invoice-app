import styled from 'styled-components';

export const NotFoundContainer = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const StatusContainer = styled.span`
  font-size: 11rem;
  color: var(--clr-primary-400);
  line-height: 1.2;
`;

export const StatusTextContainer = styled.span`
  font-size: 2.5rem;
  color: var(--clr-neutral-500);
`;
