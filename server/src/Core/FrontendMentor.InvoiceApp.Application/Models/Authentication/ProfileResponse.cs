﻿namespace FrontendMentor.InvoiceApp.Application.Models.Authentication;

public sealed record ProfileResponse(string UserId, string FirstName, string LastName, string Email, string Avatar);
