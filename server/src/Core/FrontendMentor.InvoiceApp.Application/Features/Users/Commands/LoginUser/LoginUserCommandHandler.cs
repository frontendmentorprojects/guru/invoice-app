using FrontendMentor.InvoiceApp.Application.Abstractions.Authentication;
using FrontendMentor.InvoiceApp.Application.Models.Authentication;
using FrontendMentor.InvoiceApp.Application.Services.Validation;
using FrontendMentor.InvoiceApp.Application.Utility;
using Microsoft.Extensions.Logging;

namespace FrontendMentor.InvoiceApp.Application.Features.Users.Commands.LoginUser;

public sealed class LoginUserCommandHandler: CommandHandler<LoginUserCommand, LoginResponse>
{
    private readonly IAuthenticationService _authenticationService;
    public LoginUserCommandHandler(ILogger<LoginUserCommandHandler> logger, IValidatorProvider validatorProvider, IAuthenticationService authenticationService)
        : base(logger, validatorProvider)
    {
        _authenticationService = authenticationService;
    }

    protected override async Task<LoginResponse> OnHandle(LoginUserCommand request, CancellationToken cancellationToken)
    {
        var (email, password) = request;
        return await _authenticationService.LoginAsync(new LoginRequest(email, password));
    }
}
