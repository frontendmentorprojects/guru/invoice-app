import React from 'react';

// A wrapper for "JSON.parse()" to support "undefined" value
function parseJSON<T>(json: string | null): T | undefined {
  try {
    return json === 'undefined' ? undefined : (JSON.parse(json ?? '') as T);
  } catch {
    // eslint-disable-next-line no-console
    console.log('JSON parsing error on', { json });
  }

  return undefined;
}

function loadStoredValue<T>(key: string, value: T) {
  try {
    const item = localStorage.getItem(key);
    return item ? (parseJSON(item) as T) : value;
  } catch (error) {
    // eslint-disable-next-line no-console
    console.warn(`Error reading localStorage key '${key}'`, error);
  }

  return value;
}

export default function useLocalStorage<T>(key: string, initialValue: T): [T, React.Dispatch<T>] {
  const [storedValue, setStoredValue] = React.useState<T>(loadStoredValue(key, initialValue));

  const setValue = (value: T | ((value: T) => T)) => {
    try {
      const valueToStore = value instanceof Function ? value(storedValue) : value;
      localStorage.setItem(key, JSON.stringify(valueToStore));
      setStoredValue(valueToStore);
    } catch (error) {
      // eslint-disable-next-line no-console
      console.warn(`Error setting localStorage key '${key}'`, error);
    }
  };

  React.useEffect(() => setStoredValue(loadStoredValue(key, storedValue)), [key, storedValue]);

  React.useEffect(() => {
    const handleChange = () => {
      const newValue = loadStoredValue(key, storedValue);
      setStoredValue(newValue);
    };

    window.addEventListener('storage', handleChange);
  }, [key, storedValue]);

  return [storedValue, setValue];
}
