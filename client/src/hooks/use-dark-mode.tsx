import useLocalStorage from './use-local-storage';
import useMedia from './use-media';

export default function useDarkMode() {
  const prefersDarkMode = useMedia(['(prefers-color-scheme: dark)'], [true], false);
  const [isDarkMode, setDarkMode] = useLocalStorage('dark-mode-enabled', prefersDarkMode ?? false);

  return { isDarkMode, toggle: () => setDarkMode(!isDarkMode) };
}
