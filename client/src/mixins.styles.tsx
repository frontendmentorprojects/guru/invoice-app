import { css } from 'styled-components';

export const respond = {
  tablet: (...args: Parameters<typeof css>) => css`
    @media (min-width: 48em) {
      ${css(...args)}
    }
  `,
  desktop: (...args: Parameters<typeof css>) => css`
    @media (min-width: 90em) {
      ${css(...args)}
    }
  `
};
