import React, { ReactNode } from 'react';
import { ThemeProvider as StyledThemeProvider } from 'styled-components';

import useDarkMode from '../hooks/use-dark-mode';

import { dark, light } from '../themes.styles';

const ThemeContext = React.createContext<{ isDarkMode: boolean; toggle: () => void } | undefined>(undefined);

function ThemeProvider({ children }: { children: ReactNode }) {
  const { isDarkMode, toggle } = useDarkMode();

  const theme = isDarkMode ? dark : light;

  const value = React.useMemo(() => ({ isDarkMode, toggle }), [isDarkMode, toggle]);

  return (
    <ThemeContext.Provider value={value}>
      <StyledThemeProvider theme={theme}>{children}</StyledThemeProvider>
    </ThemeContext.Provider>
  );
}

function useTheme() {
  const context = React.useContext(ThemeContext);
  if (context === undefined) {
    throw new Error('useTheme must be used within a ThemeProvider');
  }

  return context;
}

export { ThemeProvider, useTheme };
