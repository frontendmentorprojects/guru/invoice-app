import { useFormikContext } from 'formik';
import React, { ButtonHTMLAttributes } from 'react';

import { BaseButton, PrimaryButton } from './button.styles';

import Spinner from '../spinner/spinner.component';

export enum ButtonTypes {
  Primary = 'primary'
}

type ButtonProps = {
  buttonType?: ButtonTypes;
  children: React.ReactNode;
} & ButtonHTMLAttributes<HTMLButtonElement>;

function getButton(buttonType: ButtonTypes = ButtonTypes.Primary): typeof BaseButton {
  return {
    [ButtonTypes.Primary]: PrimaryButton
  }[buttonType];
}

function getButtonContent(children: React.ReactNode, busy = false) {
  return (
    <>
      {busy && <Spinner />}
      {!busy && children}
    </>
  );
}

export default function Button({ buttonType, children, ...otherProps }: ButtonProps) {
  const CustomButton = getButton(buttonType);

  return <CustomButton {...otherProps}>{getButtonContent(children)}</CustomButton>;
}

export function SubmitButton<TModel>({ buttonType, children, ...otherProps }: ButtonProps) {
  const context = useFormikContext<TModel>();

  const CustomButton = getButton(buttonType);

  return (
    <CustomButton
      type="submit"
      {...otherProps}
      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      onClick={async () => {
        await context.submitForm();
        context.setSubmitting(false);
      }}
      disabled={context.isSubmitting || context.isValidating || !context.isValid}
    >
      {getButtonContent(children, context.isSubmitting)}
    </CustomButton>
  );
}
