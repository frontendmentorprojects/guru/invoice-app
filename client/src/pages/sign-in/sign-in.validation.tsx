import * as Yup from 'yup';

export const SignInValidationSchema = Yup.object().shape({
  email: Yup.string().email().required("Can't be empty"),
  password: Yup.string().required("Can't be empty")
});
