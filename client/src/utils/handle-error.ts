import axios from 'axios';
import { toast } from 'react-toastify';

export default function handleError(error: unknown) {
  if (axios.isAxiosError(error)) {
    let msg = '';
    if (error.code === 'ERR_NETWORK') msg = 'A network related error occurred';
    if (error.response?.status === 401) {
      if ((error.request as XMLHttpRequest).responseURL.includes('/auth/login')) {
        msg = 'username and/or password is incorrect';
      } else {
        window.history.replaceState({}, '', '/auth/login');
      }
    }

    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument, @typescript-eslint/no-unsafe-member-access
    if (error.response?.status === 400) msg = Object.values(error.response.data?.errors).join('\n');
    if (error.response?.status === 500) msg = 'An unexpected error occurred. Please try again later...';

    toast.error(msg, {
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true
    });
  }
}
