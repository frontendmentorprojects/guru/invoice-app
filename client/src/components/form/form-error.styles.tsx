import styled from 'styled-components';

export const Error = styled.span`
  position: absolute;
  right: 0;
  font-size: 0.75rem;
  font-weight: 500;
  color: var(--clr-accent-400);
`;
